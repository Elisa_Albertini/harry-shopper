<?php

class DatabaseHelper
{
  private $db;

  public function __construct($servername, $username, $password, $dbname)
  {
    $this->db = new mysqli($servername, $username, $password, $dbname);
    if ($this->db->connect_error) {
      die("Connesione fallita al db");
    }
  }

  public function getSpecificProducts($tipo)
  {
    if ($tipo == "none") {
      $stmt = $this->db->prepare("SELECT codProdotto, nomeProdotto, descrizione, quantità, prezzo, foto
      FROM prodotto");
      $stmt->execute();
    } else {
      $stmt = $this->db->prepare("SELECT codProdotto, nomeProdotto, descrizione, quantità, prezzo, foto
      FROM prodotto
      WHERE nomeTipologia = ?");
      $stmt->bind_param("s", $tipo);
    }

    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
  }

  public function checkLogin($username, $password, $ruolo)
  {
    $stmt = $this->db->prepare("SELECT username, password, nome, cognome, punti, venditore FROM user WHERE username = ? AND venditore = ?");
    $password = crypt($password, '$5$rounds=5000$hogwartsisthebest$');
    $stmt->bind_param("ss", $username, $ruolo);
    $stmt->execute();
    $result = $stmt->get_result();
    $result = $result->fetch_all(MYSQLI_ASSOC);
    if(count($result) > 0 && hash_equals($result[0]["password"], $password)){
        return $result;
    }else{
        $array = array();
        return $array;
    }
}

  /*Funzione per singolo prodotto per pagina prodotto, con parametro?*/
  public function infoProdotto($codice)
  {
    $stmt = $this->db->prepare("SELECT *
                                FROM prodotto P
                                WHERE P.codProdotto = ? LIMIT 1");
    $stmt->bind_param("s", $codice);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
  }

  //cerchiamo i prodotti acquistati
  public function prodottiAcquistati($username)
  {
    $stmt = $this->db->prepare("SELECT P.codProdotto, nomeProdotto, data_ora, statoConsegna, indirizzo
                                FROM prodotto_acquistato PA, prodotto P
                                WHERE username = ?
                                AND PA.codProdotto = P.codProdotto");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
  }

  public function prodottiAcquistatiNonSpediti()
  {
    $stmt = $this->db->prepare("SELECT username, P.codProdotto, nomeProdotto, data_ora, statoConsegna, indirizzo
                                FROM prodotto_acquistato PA, prodotto P
                                WHERE PA.codProdotto = P.codProdotto
                                AND statoConsegna = 'In attesa di essere spedito'");
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
  }

  public function prodottiCarrello($username)
  {
    $stmt = $this->db->prepare("SELECT nomeProdotto, quantitàInserita, prezzo, data_ora, P.codProdotto
                                FROM prodotto_carrello PC, prodotto P
                                WHERE userCarrello = ?
                                AND PC.prodottoCarrello = P.codProdotto");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
  }

  public function totaleCarrello($username)
  {
    $prodotti = $this->prodottiCarrello($username);
    $totale = 0;
    foreach ($prodotti as $prod) {
      $totale += ($prod["prezzo"] * $prod["quantitàInserita"]);
    }

    if (isset($_POST["checkPunti"])) {
      $sconto = floor($_SESSION["punti"] / 50);
      $oldSconto = $sconto;
      while ($sconto >= 1) {
        if ($totale >= 10) {
          $totale = $totale - 5;
          $sconto--;
        } else {
          break;
        }
      }
      $_SESSION["punti_scontati"] = ($oldSconto - $sconto);
    }
    return $totale;
  }

  public function inserisciCarrello($data, $quantità, $user, $prodotto)
  {
    $stmt = $this->db->prepare("SELECT COUNT(*) as num
                                FROM prodotto_carrello
                                WHERE userCarrello = ?
                                AND prodottoCarrello = ?");
    $stmt->bind_param("ss", $user, $prodotto);
    $stmt->execute();
    $result = $stmt->get_result();
    $result = $result->fetch_all(MYSQLI_ASSOC);

    if ($result[0]["num"] == 0) {
      $stmt = $this->db->prepare("INSERT INTO prodotto_carrello
      (data_ora, quantitàInserita, userCarrello, prodottoCarrello)
      VALUES (?, ?, ?, ?)");
      $stmt->bind_param("ssss", $data, $quantità, $user, $prodotto);
      $stmt->execute();
    } else {
      $stmt = $this->db->prepare("SELECT quantitàInserita
                                  FROM prodotto_carrello
                                  WHERE userCarrello = ?
                                  AND prodottoCarrello = ?");
      $stmt->bind_param("ss", $user, $prodotto);
      $stmt->execute();
      $result = $stmt->get_result();
      $result = $result->fetch_all(MYSQLI_ASSOC);

      $stmt = $this->db->prepare("DELETE FROM prodotto_carrello
                                WHERE userCarrello = ?
                                AND prodottoCarrello = ?");
      $stmt->bind_param("ss",  $user, $prodotto);
      $stmt->execute();

      $tot = 0;
      foreach ($result as $qtà) {
        $tot += $qtà["quantitàInserita"];
      }
      $newQtà = $quantità + $tot;
      $stmt = $this->db->prepare("INSERT INTO prodotto_carrello
      (data_ora, quantitàInserita, userCarrello, prodottoCarrello)
      VALUES (?, ?, ?, ?)");
      $stmt->bind_param("ssss", $data, $newQtà, $user, $prodotto);
      $stmt->execute();
    }
  } //Funzione per la gestione della notifica relativa all'acquisto di un prodotto
  private function addPurchaseNotification($user, $nomeProdotto)
  {
    $testo = "Hai acquistato: " . $nomeProdotto;
    $stmt = $this->db->prepare("INSERT INTO notifica
                              (notifica, username)
                              VALUES (?, ?)");
    $stmt->bind_param("ss", $testo, $user);
    $stmt->execute();
  }

  //Funzione per la gestione della notifica relativa allo stato di consegna di un prodotto
  private function addStateNotification($user, $nomeProdotto, $statoConsegna)
  {
    $testo = $nomeProdotto . " è stato " . $statoConsegna;
    $stmt = $this->db->prepare("INSERT INTO notifica
                              (notifica, username)
                              VALUES (?, ?)");
    $stmt->bind_param("ss", $testo, $user);
    $stmt->execute();
  }

  //Funzione per consultare tutte le notifiche e poi eliminarle
  public function getEveryNotification($username)
  {
    $stmt = $this->db->prepare("SELECT *
                                  FROM notifica
                                  WHERE username = ?");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();
    $result = $result->fetch_all(MYSQLI_ASSOC);

    $stmt = $this->db->prepare("DELETE FROM notifica
                                WHERE username = ?");
    $stmt->bind_param("s", $username);
    $stmt->execute();

    return $result;
  }
  //dice se si deve aggiornare il popup (c'è nuovo acquisto o il prodotto deve cambiare stato)
  public function toNotify($username)
  {
    $stmt = $this->db->prepare("SELECT COUNT(*) as count
                                  FROM notifica
                                  WHERE username = ?");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();
    $result = $result->fetch_all(MYSQLI_ASSOC);

    return $result[0]["count"] !== 0;
  }

  //modifica stato prodotto in prodotto acquistato
  public function changeProductState($username, $data_ora, $codProdotto, $newState)
  {
    $stmt = $this->db->prepare("UPDATE prodotto_acquistato
                                SET statoConsegna = ?
                                WHERE username = ? AND data_ora = ? AND codProdotto = ?");
    $stmt->bind_param("ssss", $newState, $username, $data_ora, $codProdotto);
    $stmt->execute();

    $nome = ($this->infoProdotto($codProdotto))[0]["nomeProdotto"];
    $this->addStateNotification($username, $nome, $newState);
  }

  public function inserisciAcquistati($data, $quantità, $user, $prodotto, $indirizzo)
  {
    $nome = ($this->infoProdotto($prodotto))[0]["nomeProdotto"];
    $this->addPurchaseNotification($user, $nome);
    $stato = "In attesa di essere spedito";
    $stmt = $this->db->prepare("INSERT INTO prodotto_acquistato
                              (data_ora, quantitàComprata, statoConsegna, username, codProdotto, indirizzo)
                              VALUES (?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("ssssss", $data, $quantità, $stato, $user, $prodotto, $indirizzo);
    $stmt->execute();
  }

  public function points($user)
  {
    if (isset($_SESSION["punti_scontati"]) && $_SESSION["punti_scontati"]>0) {
      $punti = $_SESSION["punti_scontati"] * 50;
      $new = $_SESSION["punti"] - $punti;
      $_SESSION["punti"] = $new;
      $stmt = $this->db->prepare("UPDATE `user` SET `punti` = ? WHERE `user`.`username` = ?");
      $stmt->bind_param("ss", $new, $user);
      $stmt->execute();
      unset($_SESSION["punti_scontati"]);
    } else {
      $_SESSION["punti"] = $this->setPoint($_SESSION["username"]);
    }
  }

  public function eliminaCarrello($data, $user, $prodotto)
  {
    $stmt = $this->db->prepare("DELETE FROM prodotto_carrello
                                WHERE data_ora = ?
                                AND userCarrello = ?
                                AND prodottoCarrello = ?");
    $stmt->bind_param("sss", $data, $user, $prodotto);
    $stmt->execute();
  }

  public function inserisciUtente($username, $nome, $cognome, $email, $password, $indirizzo)
  {
    $venditore = 0;
    $stmt = $this->db->prepare("SELECT COUNT(*) as countUser
                                  FROM user
                                  WHERE username = ?");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();
    $result = $result->fetch_all(MYSQLI_ASSOC);
    if ($result[0]["countUser"] == 0) {
      $passwordCr = crypt($password, '$5$rounds=5000$hogwartsisthebest$');
      $stmt = $this->db->prepare("INSERT INTO user
                                (username, nome, cognome, email, password, venditore, indirizzoPrincipale)
                                VALUES
                                (?, ?, ?, ?, ?, ?, ?)");
      $stmt->bind_param("sssssss", $username, $nome, $cognome, $email, $passwordCr, $venditore, $indirizzo);
      $stmt->execute();
      return true;
    } else {
      return false;
    }
  }

  public function updateMagazzino($qtà, $codProd)
  {
    $stmt = $this->db->prepare("UPDATE prodotto
                                SET quantità = ?
                                WHERE prodotto.codProdotto = ?");
    $stmt->bind_param("ss", $qtà, $codProd);
    $stmt->execute();
  }

  public function qtàProdotto($codProd)
  {
    $stmt = $this->db->prepare("SELECT quantità
                                FROM prodotto
                                WHERE codProdotto = ?");
    $stmt->bind_param("s", $codProd);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC)[0]["quantità"];
  }

  public function setPoint($username)
  {
    $point = $_SESSION["totale"] / 10;
    $point = floor($point);

    $stmt = $this->db->prepare("SELECT punti
                                FROM user
                                WHERE username = ?");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();
    $result = $result->fetch_all(MYSQLI_ASSOC);

    $tot = $point + $result[0]["punti"];

    $stmt = $this->db->prepare("UPDATE user
                                SET punti = ?
                                WHERE username = ?");
    $stmt->bind_param("ss", $tot, $username);
    $stmt->execute();

    return $tot;
  }

  public function getQuantity($cod)
  {
    $qtà = $this->infoProdotto($cod);
    $qtà = $qtà[0]["quantità"];

    $stmt = $this->db->prepare("SELECT SUM(quantitàInserita) as tot
                                FROM prodotto_carrello
                                WHERE prodottoCarrello = ?");
    $stmt->bind_param("s", $cod);
    $stmt->execute();
    $result = $stmt->get_result();
    $result = $result->fetch_all(MYSQLI_ASSOC);

    if (!empty($result)) {
      $qtàCarrello = $result[0]["tot"];
      return ($qtà - $qtàCarrello);
    } else {
      return ($qtà);
    }
  }

  private function getData($cod)
  {
    $stmt = $this->db->prepare("SELECT data_ora
                                FROM frasi_giorno
                                WHERE codice = ?");
    $stmt->bind_param("s", $cod);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC)[0]["data_ora"];
  }

  private function getFraseFromData($data)
  {
    $stmt = $this->db->prepare("SELECT COUNT(*) as cnt
                                FROM frasi_giorno
                                WHERE data_ora = ?");
    $stmt->bind_param("s", $data);
    $stmt->execute();
    $result = $stmt->get_result();
    $result = $result->fetch_all(MYSQLI_ASSOC);
    if($result[0]["cnt"] > 0){
      $stmt = $this->db->prepare("SELECT nome
      FROM frasi_giorno
      WHERE data_ora = ?");
      $stmt->bind_param("s", $data);
      $stmt->execute();
      $result = $stmt->get_result();
      $result = $result->fetch_all(MYSQLI_ASSOC);
      return $result[0]["nome"];
    }else{
      return -1;
    }
  }

  public function getQuotes($data)
  {
    $stmt = $this->db->prepare("SELECT testo
                                FROM frasi_giorno
                                WHERE data_ora = ?");
    $stmt->bind_param("s", $data);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_all(MYSQLI_ASSOC)[0]["testo"];
  }

  public function getFrase()
  {
    if (!isset($_SESSION["data"])) {
      $_SESSION["data"] = date("Y-m-d");
    }
    if ($this->getFraseFromData(date("Y-m-d")) === -1 || $_SESSION["data"] !== date("Y-m-d")) {
      $cod = rand(1, 4);
      while ($this->getData($cod) === date("Y-m-d", strtotime("-1 days"))) {
        $cod = rand(1, 4);
      }

      $data = date("Y-m-d");

      $stmt = $this->db->prepare("UPDATE `frasi_giorno` SET `data_ora` = ? WHERE `frasi_giorno`.`codice` = ?");
      $stmt->bind_param("ss", $data, $cod);
      $stmt->execute();

      $_SESSION["data"] = date("Y-m-d");
    }
    return $this->getFraseFromData(date("Y-m-d"));
  }
  public function aggiungiNuovoProdotto($nomeProdotto, $descrizione, $quantità, $prezzo, $foto, $nomeTipologia)
  {
    $qt = 1;
    $pz = 1;
    if ($quantità <= 0) {
      $_SESSION["error_qtà_new"] = "La quantità inserita non è valida";
      $qt = 0;
    }
    if ($prezzo <= 0) {
      $_SESSION["error_prezzo"] = "Il prezzo inserito non è valido";
      $pz = 0;
    }

    $stmt = $this->db->prepare("SELECT  COUNT(*) as tipo
                                  FROM tipologia
                                  WHERE nomeTipologia = ?");
    $stmt->bind_param("s", $nomeTipologia);
    $stmt->execute();
    $result = $stmt->get_result();
    $result =  $result->fetch_all(MYSQLI_ASSOC)[0]["tipo"];

    if ($result > 0 && $qt && $pz) {
      $stmt = $this->db->prepare("INSERT INTO prodotto
      (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`)
      VALUES (NULL, ?, ?, ?, ?, ?, ?)");
      $stmt->bind_param("ssssss", $nomeProdotto, $descrizione, $quantità, $prezzo, $foto, $nomeTipologia);
      $stmt->execute();
    } else {
      $_SESSION["error_tipo"] = "Tipologia non esistente";
    }
  }

  public function updateProdotto($codProdotto, $qtà)
  {

    $stmt = $this->db->prepare("SELECT quantità
                                  FROM prodotto
                                  WHERE codProdotto = ?");
    $stmt->bind_param("s", $codProdotto);
    $stmt->execute();
    $result = $stmt->get_result();
    $result =  $result->fetch_all(MYSQLI_ASSOC)[0]["quantità"];

    if ($qtà >= 0) {
      unset($_SESSION["error_qtà"]);
      $qtà = $qtà + $result;
      $stmt = $this->db->prepare("UPDATE prodotto
                                  SET quantità = ?
                                  WHERE codProdotto = ?");
      $stmt->bind_param("ss", $qtà, $codProdotto);
      $stmt->execute();
    } else {
      $_SESSION["error_qtà"] = "La quantità inserita non è valida";
    }
  }

  public function updateIndirizzoUser($username, $newIndirizzo)
  {
    $stmt = $this->db->prepare("UPDATE user
                              SET indirizzoPrincipale = ?
                              WHERE username = ?");
    $stmt->bind_param("ss", $newIndirizzo, $username);
    $stmt->execute();
  }

  public function getIndirizzo($username)
  {
    $stmt = $this->db->prepare("SELECT indirizzoPrincipale
                              FROM user
                              WHERE username = ?");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();
    return  $result->fetch_all(MYSQLI_ASSOC)[0]["indirizzoPrincipale"];
  }
}
