/*Inserimento delle tipologie*/

/*Scherzi*/
INSERT INTO `tipologia` (`nomeTipologia`) VALUES ('Scherzi');

/*Animali*/
INSERT INTO `tipologia` (`nomeTipologia`) VALUES ('Animali');

/*Pozioni*/
INSERT INTO `tipologia` (`nomeTipologia`) VALUES ('Pozioni');

/*Cancelleria*/
INSERT INTO `tipologia` (`nomeTipologia`) VALUES ('Cancelleria');

/*Inserimento dei prodotti*/

/*Merendine Marinare*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`)
VALUES (NULL, 'Merendine Marinare', 'Queste sono una varietà di merendine che, quando vengono mangiate, permettono al soggetto di mostrare i sintomi di alcune malattie, potendo così allontanarsi dalla classe o saltare le lezioni. Le caramelle hanno due metà: una arancione, che provoca il finto malessere, e una viola, che è l’antidoto e va consumata una volta fuori dalla classeper far scomparire i sintomi e potersi così dedicare allo svago.', '50', '2', '1.jpg', 'Scherzi');

/*Orecchie Oblunghe*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Orecchie Oblunghe', 'Mettendo un’estremità di questi lunghi fili color carne in un orecchio e l’altra estremità sotto una porta o finestra di una casa o locale si può ascoltare e origliare qualunque cosa si dica e si senta dall’altra parte di questa porta o finestra.', '40', '5', '2.jpg', 'Scherzi');

/*Polvere Buiopesto Peruviana*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Polvere Buiopesto Peruviana', 'Ai Tiri Vispi troverete la Polvere Buiopesto Peruviana, che è una polvere nera come la pece proveniente dal Perù, che consente a chi la usa di creare un’atmosfera talmente densa e nera da non fare passare neanche un filo di luce.', '30', '6', '3.jpg', 'Scherzi');

/*Puffola Pigmea*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Puffola Pigmea', 'Le Puffole Pigmee sono graziosi animaletti a batuffolo col pelo colorato in tutte le gradazioni del rosa e del viola.', '15', '10', '4.jpg', 'Animali');

/*Pasticche Vomitose*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Pasticche Vomitose', 'Le Pasticche Vomitose, se mangiate, fanno vomitare violentemente all’istante. Possono causare problemi se chi le mangia vomita così tanto da non riuscire a ingoiare l’antidoto.', '150', '2', '5.jpg', 'Scherzi');

/*Filtro d'amore*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Filtro d\'amore', 'Filtro d\'amore da somministrare alla persona da far innamorare. Durata dell\'effetto: 24h.', '50', '15', '6.jpg', 'Pozioni');

/*Piuma Autocorregente*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Piuma Autocorregente', 'La Piuma Autocorregente corregge automaticamente gli errori che si commettono mentre si sta scrivendo; ha però un effetto di breve durata e quando l’incantesimo sta per esaurirsi si finisce per commettere errori giganteschi.', '150', '2', '7.jpg', 'Cancelleria');

/*Detonatore Abbindolante*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Detonatore Abbindolante', 'I Detonatori Abbindolanti sono piccoli oggetti simili a clacson dotati di gambe che, se lasciati cadere a terra, scappano via velocemente moltiplicandosi emettendo un botto e facendo fuoruscire del fumo nero.', '300', '3', '8.jpg', 'Scherzi');

/*Cappello Scudo*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Cappello Scudo', 'I Cappelli Scudo lanciano un Sortilegio Scudo a chi li indossa, parando o alleviando i danni delle fatture o degli incantesimi e respingendoli (se deboli) a colui che li ha lanciati. Esistono in altre forme, come i Mantelli scudo e i Guanti scudo.', '50', '9', '9.jpg', 'Scherzi');

/*Bacchetto Trabocchetto*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Bacchetta Trabocchetto', 'Le Bacchette Trabocchetto sembrano normali bacchette, ma se agitate si trasformano in polli di gomma o mutande.', '100', '2', '10.jpg', 'Scherzi');

/*Annullaforuncoli Garantito Dieci Secondi*/
INSERT INTO `prodotto` (`codProdotto`, `nomeProdotto`, `descrizione`, `quantità`, `prezzo`, `foto`, `nomeTipologia`) VALUES (NULL, 'Annullaforuncoli Garantito Dieci Secondi', 'L’Annullaforuncoli Garantito Dieci Secondi è una pomata in grado di eliminare brufoli e punti neri in dieci secondi.', '50', '4', '11.jpg', 'Pozioni');

/*Inserimento degli utenti*/

/*Utente "AmeliaLaStrega"*/
INSERT INTO `user` (`username`, `nome`, `cognome`, `email`, `password`, `venditore`,`indirizzoPrincipale`) VALUES ('AmeliaLaStrega', 'Amelia', 'LaStrega', 'AmeliaLaStrega@hogwarts.it', '$5$rounds=5000$hogwartsisthebes$HuylYHijeqgHFpXavm6yippJcd69RSsp7JKjmaFVWFB', '0',"Hogwarts");

/*Venditore "SonoFredMamma"*/
INSERT INTO `user` (`username`, `nome`, `cognome`, `email`, `password`, `venditore`,`indirizzoPrincipale`) VALUES ('SonoFredMamma', 'Fred', 'Weasley', 'FredWeasley@hogwarts.it', '$5$rounds=5000$hogwartsisthebes$eaEnGd55fzqEAuRdeytK5bfepUNZJaZxXPm5Y/BK9a6', '1', "Hogwarts");

/*Frasi giorno*/
INSERT INTO `frasi_giorno` (`codice`, `nome`, `testo`, `data_ora`) VALUES (NULL, 'Albus', 'It is our choices that show what we truly are far more than our abilities. - Albus Dumbledore -', NULL);
INSERT INTO `frasi_giorno` (`codice`, `nome`, `testo`, `data_ora`) VALUES (NULL, 'Harry', 'Time will not slow down when something unpleasant lies ahead. - Harry Potter - ', NULL);
INSERT INTO `frasi_giorno` (`codice`, `nome`, `testo`, `data_ora`) VALUES (NULL, 'Luna', 'I\'ll just go down and have some pudding and wait for it all to turn up... It always does in the end. - Luna Lovegood - ', NULL);
INSERT INTO `frasi_giorno` (`codice`, `nome`, `testo`, `data_ora`) VALUES (NULL, 'Sirius', 'If you want to know what a man\'s like, take a good look at how he treats his inferiors, not his equals. - Sirius Black -', NULL);
