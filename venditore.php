<?php
session_start();
require("functions.php");
if (!isset($_SESSION["username"])) : ?>
  <?php set_url("login.php"); ?>
<?php
elseif ($_SESSION["ruolo"]) :
?>

  <!DOCTYPE html>
  <html>

  <head>

    <title>Harry Shopper - Gestione Prodotti</title>
    <?php
    require("php/bootstrap.php");
    $products = $dbh->getSpecificProducts("none");
    ?>

    <link rel="stylesheet" href="css\venditore.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>

  <body>

    <form method="post">
      <h2>Aggiunta Prodotto</h2>
      <p><?php
          if (isset($_SESSION["error_tipo"])) {
            echo $_SESSION["error_tipo"];
            echo "<br>";
            unset($_SESSION["error_tipo"]);
          }
          if (isset($_SESSION["error_qtà_new"])) {
            echo $_SESSION["error_qtà_new"];
            echo "<br>";
            unset($_SESSION["error_qtà_new"]);
          }
          if (isset($_SESSION["error_prezzo"])) {
            echo $_SESSION["error_prezzo"];
            echo "<br>";
            unset($_SESSION["error_prezzo"]);
          }
          ?></p>
      <div class="container">
        <label for="nome">Nome</label>
        <input type="text" id="nome" placeholder="Inserire nome" name="nome" required>
        <br>
        <label for="descrizione">Descrizione</label>
        <input type="text" id="descrizione" placeholder="Inserire descrizione" name="descrizione" required>
        <br>
        <label for="quantità">Quantità</label>
        <input type="text" id="quantità" placeholder="Inserire quantità" name="quantità" required>
        <br>
        <label for="Prezzo">Prezzo</label>
        <input type="text" id="Prezzo" placeholder="Inserire prezzo" name="prezzo" required>
        <br>
        <label for="LinkFoto">Link Foto</label>
        <input type="text" id="LinkFoto" placeholder="Inserire link" name="linkFoto" required>
        <br>
        <label for="tipo">Tipologia</label>
        <input type="text" id="tipo" placeholder="Inserire tipologia" name="tipologia" required>
        <br>
        <br>
        <button type="submit">Aggiungi Prodotto</button>
      </div>
    </form>

    <form method="post">
      <h2>Rifornimento Prodotto</h2>
      <label><?php if (isset($_SESSION["error_qtà"])) {
                echo $_SESSION["error_qtà"];
                echo "<br>";
                unset($_SESSION["error_qtà"]);
              } ?> </label>
        <div class="container">
          <label for="prod">Prodotto:</label>

          <select id="prod" name="codice">
            <?php foreach ($products as $prod) : ?>
              <option value="<?php echo $prod["codProdotto"] ?>"><?php echo $prod["nomeProdotto"] ?></option>
            <?php endforeach ?>
          </select>
          <br>
          <label for="stato">Quantità</label>
          <input type="text" id="stato" placeholder="Inserire quantità" name="quantità" required>

          <button type="submit">Rifornisci</button>
        </div>
    </form>
    <a href="venditore_spedizioni.php">Vai alla gestione spedizioni</a><br>
    <a href="login.php">Log out</a>
    <?php require("gestione_prodotti.php"); ?>
  </body>
<?php elseif (!$_SESSION["ruolo"]) : ?>
  <?php set_url("profilo_utente.php"); ?>
<?php endif; ?>
