<?php
if (isset($_POST["nome"])&& isset($_POST["descrizione"]) && isset($_POST["quantità"]) && isset($_POST["prezzo"]) && isset($_POST["linkFoto"]) && isset($_POST["tipologia"])) {
    $dbh->aggiungiNuovoProdotto($_POST["nome"], $_POST["descrizione"], $_POST["quantità"], $_POST["prezzo"], $_POST["linkFoto"], $_POST["tipologia"]);
    unset($_POST["submit_new_prod"]);
    echo "<meta http-equiv='refresh' content='0'>";
}

if (isset($_POST["codice"]) && isset($_POST["quantità"])) {
    $dbh->updateProdotto($_POST["codice"], $_POST["quantità"]);
    unset($_POST["submit_change_prod"]);
    echo "<meta http-equiv='refresh' content='0'>";
}
