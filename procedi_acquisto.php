<?php
if(isset($_SESSION["username"]) && isset($_POST["indirizzo"]))
{
    if(!empty($_POST["indirizzo"])){
      $dbh->updateIndirizzoUser($_SESSION["username"], $_POST["indirizzo"]);
    }
    $indirizzo = $dbh ->getIndirizzo($_SESSION["username"]);
    $prodotti = $dbh->prodottiCarrello($_SESSION["username"]);
    $dbh->points($_SESSION["username"]);
    foreach($prodotti as $prod){
        $qtàAcquistata = $prod["quantitàInserita"];

        $qtàTotale = $dbh->qtàProdotto($prod["codProdotto"]);

        $nuovaQtà = $qtàTotale - $qtàAcquistata;

        $dbh->updateMagazzino($nuovaQtà, $prod["codProdotto"]);

        //inserisci in prodotti comprati
        $dbh->inserisciAcquistati(date("Y-m-d H:i:sa"), $prod["quantitàInserita"], $_SESSION["username"], $prod["codProdotto"], $indirizzo);


        //togli dai prodotti nel carrello
        $dbh->eliminaCarrello($prod["data_ora"], $_SESSION["username"], $prod["codProdotto"]);
    }
    echo "<meta http-equiv='refresh' content='0'>";
}
?>
