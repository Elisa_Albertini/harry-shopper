<!DOCTYPE html>
<html>

<head>
    <title>Harry Shopper - Riepilogo</title>
    <?php
    require("dipendenze/dipendenze_head.php");
    require("functions.php");
    if (!isset($_SESSION["username"])) : ?>
    <?php set_url("login.php");
    elseif (!$_SESSION["ruolo"]) :
    ?>
        <link rel="stylesheet" href="css\riepilogo.css">
</head>

<body>

    <?php
        require("componenti/navbar/navbar.php");
        require("php/bootstrap.php");

        $product = $dbh->prodottiAcquistati($_SESSION["username"]);
        $messaggioErrore = '0';
        if (empty($product)) {
            $messaggioErrore = "Nessun prodotto acquistato";
        }
    ?>

    <h1 class="m-4">Riepilogo Ordini</h1>
    <br>

    <div class="row m-2">
        <div class="col">
            <div class="list-group m-4">
                <label><?php if ($messaggioErrore != '0') {
                            echo $messaggioErrore;
                            $messaggioErrore = '0';
                        } ?></label>
                <?php foreach ($product as $prod) : ?>
                    <div class="m-2"></div>
                    <li class="list-group-item list-group-item-action">
                        <h2 class="mb-1"> <?php echo $prod["nomeProdotto"] ?></h2>
                        <div>
                            <small>Acquistato il </small>
                            <strong><?php echo $prod["data_ora"] ?></strong>
                        </div>
                        <div>
                            <small>Indirizzo: </small>
                            <strong><?php echo $prod["indirizzo"] ?></strong>
                        </div>
                        <div>
                            <small>Stato ordine: </small>
                            <strong><?php echo $prod["statoConsegna"] ?></strong>
                        </div>
                        <a alt="vai al prodotto" href="pagina_prodotto.php?ID=<?php echo $prod["codProdotto"] ?>" class="btn btn-primary m-3 vai">
                            VISUALIZZA
                        </a>

                    </li>
                <?php endforeach; ?>
            </div>
        </div>
    </div>



    <?php
        require("componenti/footer/footer.php");
    ?>

</body>
<?php elseif ($_SESSION["ruolo"]) : ?>
    <?php set_url("venditore.php"); ?>
<?php endif; ?>