<!DOCTYPE html>
<html>

<head>
	<title>Harry Shopper - Home</title>
	<?php
	require_once("php/bootstrap.php");
	require("dipendenze/dipendenze_head.php");
	?>
	<?php
	if(isset($_SESSION["username"]) && !$_SESSION["ruolo"] || !isset($_SESSION["username"])) :
	?>
	<script src="componenti/popup/popup.js" type="text/javascript"></script>
</head>

<body>

	<?php
	require("componenti/navbar/navbar.php");
	?>

	<br>
	<br>
	<div class="row m-0 justify-content-center">
		<div class="col m-0">
			<div>
				<h1 class="text-center">Il miglior negozio di scherzi di tutta Diagon Alley!</h1>
			</div>
		</div>
		<div class="col-md m-0">
			<img id="hero" src="image\hero_image.png" alt="cartello che punta il dito" />
		</div>
	</div>

	<br>
	<br>

	<div class="row m-3 justify-content-center">
		<div class="col-md-5 mb-4">
			<div class="pb-3">
				<h2 class="text-center">Ecco la frase del giorno</h2>
			</div>
			<img class="frasi" src="image\frasi_del_giorno\<?php echo $dbh->getFrase(); ?>.gif" alt="<?php $dbh->getQuotes(date("Y-m-d"))?>">
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-5 ">
			<h2 class="text-center">Scopri i nostri prodotti!</h2>
			<div class="slider">
				<div class="slider-image">
					<img id="scherzi" src="image\icone\scherzi.png" alt="Scherzi" title="Scherzi" />
					<img id="pozioni" src="image\icone\pozioni.png" alt="Pozioni" title="Pozioni" />
					<img id="animali" src="image\icone\animali.png" alt="Animali" title="Animali" />
					<img id="cancelleria" src="image\icone\cancelleria.png" alt="Cancelleria" title="Cancelleria" />
				</div>
				<div class="slider-control">
					<div class="row m-3 justify-content-center">

						<div class="col justify-content-center">
							<button type="button" id="prec" class="btn"><strong class="fas fa-arrow-circle-left"></strong></button>
						</div>
						<div class="col">
							<button type="button" id="succ" class="btn"><strong class="fas fa-arrow-circle-right"></strong></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>

	<?php
	require("componenti/footer/footer.php");
	?>

</body>
<?php elseif(isset($_SESSION["username"]) && $_SESSION["ruolo"]): ?>
  <?php set_url("venditore.php");?>
<?php endif;?>
