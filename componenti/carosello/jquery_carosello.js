$(document).ready(function() {

    const slider = $(".slider-image").children("img");
    //numero di immagini nel carosello -1
    let maxImg = -1;
    //nmero dell'immagine visibile
    let selImg = 0;


    //inizializzazione 
    slider.each(function() {
        maxImg += 1;
        if ($(this).is(":first-child")) {

            selImg = maxImg;
        } else {
            $(this).hide();
        };
    });

    //clicco precedente
    $("#prec").click(function(e) {
        e.preventDefault();
        if (selImg == 0) {
            selImg = maxImg;
        } else {
            selImg--;
        };
        activeImg(selImg);
    });


    //clicco sucessivo
    $("#succ").click(function(e) {
        e.preventDefault();
        if (selImg == maxImg) {
            selImg = 0;
        } else {
            selImg++;
        };
        activeImg(selImg);
    });

    //attivo l'immagine giusta e disattivo le altre
    function activeImg(nImg) {

        let cont = 0;
        slider.each(function(cont) {

            if (cont == selImg) {
                $(this).fadeIn("slow");
            } else {
                $(this).hide();
            }
            cont++;
        });
    }

    $('.slider-image').on('click', '#scherzi', function() { window.location = "insieme_prodotti.php?TIPO=scherzi"; });
    $('.slider-image').on('click', '#pozioni', function() { window.location = "insieme_prodotti.php?TIPO=pozioni"; });
    $('.slider-image').on('click', '#animali', function() { window.location = "insieme_prodotti.php?TIPO=animali"; });
    $('.slider-image').on('click', '#cancelleria', function() { window.location = "insieme_prodotti.php?TIPO=cancelleria"; });


});