
<!-- NAVBAR -->
<nav class="navbar navbar-expand-lg">
	<a class="navbar-brand" href="#">
		<button type="button" id="sidebarCollapse" class="navbar-toggler-icon"><i alt="menu" class="fas fa-bars"></i></button>
	</a>

	<div class="collapse navbar-collapse" id="navbarTogglerDemo02">
		<div class="navbar-nav mx-auto">
			<img id="logo" src="image\logo.png" alt="logo" />
			<h2>Weasleys Wizard Wheezes</h2>
		</div>

	</div>

	<form class="form-inline my-2 my-lg-0">

		<a href="<?php
					if (isset($_SESSION["username"])) {
						echo "profilo_utente.php";
					} else {
						echo "login.php?PAG=0";
					}
					?>" class="btn"><i class="far fa-user-circle icona"></i></a>
		<a href="<?php
					if (isset($_SESSION["username"])) {
						echo "carrello.php";
					} else {
						echo "login.php?PAG=1";
					} ?>" class="btn"><i class="fas fa-shopping-cart icona"></i></a>

	</form>

</nav>

<!-- SIDEBAR -->
<div class="wrapper">
	<!-- Sidebar  -->
	<nav id="sidebar">
		<div id="dismiss">
			<i class="fas fa-arrow-left"></i>
		</div>

		<div class="sidebar-header">
			<h3>Dove vuoi andare?</h3>
		</div>

		<ul class="list-unstyled components">
			<li><a href="home.php">Home</a></li>
			<li><a href="<?php
							if (isset($_SESSION["username"])) {
								echo "profilo_utente.php";
							} else {
								echo "login.php?PAG=0";
							}
							?>">Profilo</a></li>
			<li><a href="<?php
							if (isset($_SESSION["username"])) {
								echo "carrello.php";
							} else {
								echo "login.php?PAG=1";
							} ?>">Carrello</a></li>
			<li><a href="<?php
							if (isset($_SESSION["username"])) {
								echo "riepilogo.php";
							} else {
								echo "login.php?PAG=2";
							} ?>">Riepilogo</a></li>

			<li class="active">
				<a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Prodotti ▼</a>
				<ul class="collapse list-unstyled" id="homeSubmenu">
				<li>
						<a href="insieme_prodotti.php?TIPO=none">Tutti</a>
					</li>
					<li>
						<a href="insieme_prodotti.php?TIPO=scherzi">Scherzi</a>
					</li>
					<li>
						<a href="insieme_prodotti.php?TIPO=pozioni">Pozioni</a>
					</li>
					<li>
						<a href="insieme_prodotti.php?TIPO=animali">Animali</a>
					</li>
					<li>
						<a href="insieme_prodotti.php?TIPO=cancelleria">Cancelleria</a>
					</li>
				</ul>
			</li>
		</ul>
	</nav>
</div>
<div class="overlay"></div>