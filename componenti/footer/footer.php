<!-- FOOTER -->
<footer class="footer">
	<img class="top-footer" src="image\delimitatore_footer.png" alt="cornicetta" />
	<!-- Footer -->
	<div class="page-footer font-small pt-4">
		<!-- Footer Links -->
		<div class="container-fluid text-center text-md-left">
			<!-- Grid row -->
			<div class="row">
				<!-- Grid column -->
				<div class="col-md-6 mt-md-0 mt-3">
					<!-- Content -->
					<h3 class="text-uppercase">Contatti</h3>
					<ul class="list-unstyled">
						<li>
							<strong>Elisa Albertini</strong>
							<br>
							elisa.albertini3@studio.unibo.it
						</li>
						<li>
							<strong>Eleonora Bertoni</strong>
							<br>
							eleonora.bertoni3@studio.unibo.it
						</li>
						<li>
							<strong>Samuele Bertani</strong>
							<br>
							samuele.bertani@studio.unibo.it
						</li>
					</ul>

				</div>
				<!-- Grid column -->
				<hr class="clearfix w-100 d-md-none pb-3">
				<!-- Grid column -->
				<div class="col-md-3 mb-md-0 mb-3">
					<!-- Links -->
					<h3 class="text-uppercase">Links utili</h3>
					<ul class="list-unstyled">
						<li>
							<a href="home.php">Home</a>
						</li>
						<li>
							<a href="insieme_prodotti.php">Prodotti</a>
						</li>
					</ul>
				</div>
				<!-- Grid column -->
				<div class="col-md-3 mb-md-0 mb-3">
					<!-- Links -->
					<h3 class="text-uppercase">Harry Shooper Group</h3>
					<p>Grazie per averci fatto visita!</p>
				</div>
				<!-- Grid column -->
			</div>
			<!-- Grid row -->
		</div>
		<!-- Footer Links -->
		<!-- Copyright -->
		<div class="footer-copyright text-center py-3">
			<a> Il materiale presente è usato solo a scopo didattico</a>
		</div>
		<!-- Copyright -->
	</div>
</footer>

<!-- Footer -->