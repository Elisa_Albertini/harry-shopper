<?php
session_start();
require("functions.php");
if(!isset($_SESSION["username"])) :?>
<?php set_url("login.php");?>
<?php
elseif( $_SESSION["ruolo"]) :
?>

<!DOCTYPE html>
<html>

<head>
    <title>Harry Shopper - Gestione spedizioni</title>

    <link rel="stylesheet" href="css\venditore.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

    <?php
    require("php/bootstrap.php");

    $templateParams["prodotto"] = $dbh->prodottiAcquistatiNonSpediti();
    ?>

    <h1 class="m-4">Riepilogo Ordini</h1>

    <div class="row m-2">
        <div class="col">
            <div class="list-group m-4">
                <form method="POST">
                    <ul>
                        <?php
                        $i = 0;
                        foreach ($templateParams["prodotto"] as $prod) : ?>
                            <li>

                                <input type="hidden" name="data[]" value="<?php echo $prod["data_ora"] ?>" />
                                <input type="hidden" name="utente[]" value="<?php echo $prod["username"] ?>" />
                                <input type="hidden" name="prodotto[]" value="<?php echo $prod["codProdotto"] ?>" />
                                <div class="d-flex w-100 justify-content-between">
                                    <h2 class="mb-1">Prodotto: <?php echo $prod["nomeProdotto"] ?></h2>
                                    <small>Acquistato il </small> <?php echo $prod["data_ora"] ?><br>
                                    <small>Indirizzo: </small> <?php echo $prod["indirizzo"] ?><br>
                                    <small>Stato ordine: </small> <?php echo $prod["statoConsegna"] ?><br>
                                </div>


                                <br>
                                <input class="spedisci" type="checkbox" name="spedisci[]" value="1" id=<?php echo"spedisci".$i ?>>
                                <input type='hidden' value='0' name="spedisci[]">
                                <label for=<?php echo"spedisci".$i; $i = $i + 1; ?>>Spedisci</label><br>

                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <br>
                    <button type="submit" class="btn btn-lg btn-brand btn-full-width" id="spedizione">
                        Procedi alla spedizione
                    </button>
                </form>
                <?php require("spedisci.php") ?>
            </div>
            <a href="venditore.php">Vai alla modifica prodotti</a><br>
            <a href="login.php">Log out</a>
        </div>
    </div>

</body>
<?php elseif(!$_SESSION["ruolo"]): ?>
  <?php set_url("profilo_utente.php");?>
<?php endif;?>
