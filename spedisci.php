<?php
if (isset($_POST["utente"]) && isset($_POST["data"]) && isset($_POST["prodotto"]) && isset($_POST["spedisci"])) {
    $newState = "Spedito";
    $salta = 0;
    foreach ($_POST["spedisci"] as $sped) {
        if (!$salta) {
            $spediti[] = $sped;
            if ($sped == 1) {
                $salta = 1;
            }
        } else {
            $salta = 0;
        }
    }

    $count = 0;
    foreach ($spediti as $sped) {
        if ($sped) {
            $dbh->changeProductState($_POST["utente"][$count], $_POST["data"][$count], $_POST["prodotto"][$count], $newState);
            $count++;
        }
    }
    echo "<meta http-equiv='refresh' content='0'>";
}
unset($_POST["spedisci"]);
unset($_POST["utente"]);
unset($_POST["data"]);
unset($_POST["prodotto"]);
unset($_POST["sub_spedisci"]);
?>
