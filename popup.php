<?php
session_start();
require_once("php/bootstrap.php");
?>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">C'è un gufo per te!</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php
        $notifiche = $dbh->getEveryNotification($_SESSION["username"]);
        foreach($notifiche as $value):?>
        <?php echo $value["notifica"]?>
        <br>
        <?php endforeach; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Chiudi</button>
      </div>
    </div>
  </div>
</div>
