<!DOCTYPE html>
<html>

<?php require_once("php/bootstrap.php");
if(isset($_GET["TIPO"])){
    $products = $dbh->getSpecificProducts($_GET["TIPO"]);
}else{
    $products = $dbh->getSpecificProducts("none");
}

?>

<head>
    <title>Harry Shopper - Prodotti</title>
    <?php
    require("dipendenze/dipendenze_head.php");
    ?>
  <?php if(isset($_SESSION["username"]) && !$_SESSION["ruolo"] || !isset($_SESSION["username"])) : ?>


    <link rel="stylesheet" href="css\insieme_prodotti.css">
</head>

<body>

    <?php
    require("componenti/navbar/navbar.php");
    ?>

    <div class="container-fluid">

        <div class="container mt-3">
            <div class="row row-no-gutters">
                <?php foreach ($products as $prodotto) : ?>

                    <div class="col-sm">
                        <div class="card mb-3 ">
                            <img  class="card-img-to carta" src="<?php echo UPLOAD_DIR . $prodotto["foto"]; ?>" alt="<?php echo $prodotto["nomeProdotto"]; ?>">
                            <div class="card-body">
                                <h2 class="card-title">
                                    <?php echo $prodotto["nomeProdotto"]; ?>
                                </h2>
                                <p class="card-text">

                                    <small class="text-muted">
                                        <?php echo $prodotto["quantità"] ?> rimasti
                                    </small>
                                    <br>
                                    <small class="text-muted">
                                        prezzo: <?php echo $prodotto["prezzo"] ?> galeoni
                                    </small>
                                    <div class="text-muted text-right ticky-right">
                                        <a href="pagina_prodotto.php?ID=<?php echo $prodotto["codProdotto"] ?>" class="btn btn-primary">
                                            VISUALIZZA
                                        </a>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <?php
    require("componenti/footer/footer.php");
    ?>

</body>
<?php elseif(isset($_SESSION["username"]) && $_SESSION["ruolo"]): ?>
  <?php set_url("venditore.php");?>
<?php endif;?>
