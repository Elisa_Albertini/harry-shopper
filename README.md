- Link al Design Brief e Mood Board Exercise

https://www.figma.com/file/oQH1Zu4D7d3JQU9MINNvnv/ProgettoWeb---Design-Brief-e-Mood-Board-Exercise?node-id=0%3A1

- Link alla storyboard

https://www.figma.com/file/XR4nZX0cRmwpxa1NdfsGNE/ProgettoWeb---Storyboard?node-id=0%3A1

- Creazione database

Pre creare il database copiare il contenuto di database/creazione_db.sql e incollarlo in XAMPP.
Per popolare il database copiare il contenuto di database/insert.sql e incollarlo in XAMPP dopo aver selezionato il db harry-shopper.
  Sono stati creati due utenti di prova:
      - Se si vuole entrare come venditore: Username: SonoFredMamma, Password: SonoFredMamma, cliccare sul bottone "Venditore".
      - Se si vuole entrare come utente: Username: AmeliaLaStrega, Password: AmeliaLaStrega.
  Per quanto riguarda gli utenti è possibile creare un nuovo account cliccando sul link "Non sei ancora iscritto?".

Il pdf della relazione è all'interno della repository (mockup.pdf).
