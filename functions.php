<?php
function registerLoggedUser($user)
{
    $_SESSION["username"] = $user["username"];
    $_SESSION["nome"] = $user["nome"];
    $_SESSION["cognome"] = $user["cognome"];
    $_SESSION["punti"] = $user["punti"];
    $_SESSION["ruolo"] = $user["venditore"];
}

function isUserLoggedIn()
{
    return !empty($_SESSION["username"]);
}

function set_url($url)
{
    echo "<META HTTP-EQUIV='Refresh' Content='0; URL=".$url."'>";
}
