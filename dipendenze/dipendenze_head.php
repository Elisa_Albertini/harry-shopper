<link rel="icon" type="image/png" href="image\logo.png" />
<meta charset="utf-8" />
<html lang="it">
<meta name="author" content="Elisa Albertini, Samuele Bertani, Eleonora Bertoni">
<meta name="keywords" content="scherzi, fratelli Weasley, ecc">
<meta name="description" content="Ecommerce di scherzi magici"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<script src="dipendenze\jquery-1.11.3.min.js" type="text/javascript"></script>

<!--RIFERIMENTI HTTPS-->
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
<!-- Font Awesome JS -->
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
<!-- Popper.JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<!--Font del h1 preso da google Font-->
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Langar&display=swap" rel="stylesheet">
<!--Tutte le icone sono prese da qui! ps questo è registrato con l'email samuele.bertani-->
<script src="https://kit.fontawesome.com/e80a0e69f1.js" crossorigin="anonymous"></script>




<!--RIFERIMENTI LOCALI-->

<!-- Our Custom CSS -->
<link rel="stylesheet" href="componenti\navbar\side_navbar.css">

<script src="componenti\carosello\jquery_carosello.js" type="text/javascript"></script>

<script src="componenti\navbar\jquery_menu_scomparsa.js" type="text/javascript"></script>

<link rel="stylesheet" href="componenti\style.css">

<?php
session_start();
?>
