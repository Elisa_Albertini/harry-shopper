<!DOCTYPE html>
<html>

<head>
	<title>Harry Shopper - Iscrizione</title>
	<?php
	require("functions.php");
	require("dipendenze/dipendenze_head.php");
	if(!isset($_SESSION["username"])) :
	?>
	<link rel="stylesheet" href="css\login.css">

</head>

<body>

	<?php
	require("componenti/navbar/navbar.php");
	require("php/bootstrap.php");

	unset($_SESSION["nome"]);
	unset($_SESSION["cognome"]);
	unset($_SESSION["username"]);
	?>

	<h2 id="title-form">Form Iscrizione</h2>

	<form id="scatola_login" method="post">

		<div class="imgcontainer">
			<img src="image/centrale_home.png" alt="Avatar" class="avatar">
		</div>

		<div class="container">
			<label><?php
					if (isset($_SESSION["error_iscr"])) {
						echo $_SESSION["error_iscr"];
						unset($_SESSION["error_iscr"]);
						echo "<br>";
					} ?></label>
			<label for="usname">Username</label>
			<input type="text" id = "usname" placeholder="Enter Username" name="username" required>

			<label for="psw">Password</label>
			<input type="password" id="psw" placeholder="Enter Password" name="password" required>

			<label for="uname">Nome</label>
			<input type="text" id="uname" placeholder="Enter Name" name="nome" required>

			<label for="sname">Cognome</label>
			<input type="text" id="sname" placeholder="Enter Surname" name="cognome" required>

			<label for="email">E-mail</label>
			<input type="text" id="email" placeholder="Enter E-mail" name="email" required>

			<label for="indirizzo">Indirizzo</label>
			<input type="text" id="indirizzo" placeholder="Enter Adress" name="indirizzo" required>

			<button type="submit" id="login">Iscriviti
			</button>
		</div>
	</form>
	<?php require("controllo_iscrizione.php"); ?>

	<?php
	require("componenti/footer/footer.php");
	?>

</body>
<?php elseif($_SESSION["ruolo"]): ?>
  <?php set_url("venditore.php");?>
<?php elseif(!$_SESSION["ruolo"]): ?>
  <?php set_url("login.php");?>
<?php endif;?>
