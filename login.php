<!DOCTYPE html>
<html>

<head>
	<title>Harry Shopper - Login</title>
	<link rel="stylesheet" href="css\login.css">
	<?php
	require("dipendenze/dipendenze_head.php");

	?>

</head>

<body>

	<?php
	unset($_SESSION["username"]);
	unset($_SESSION["nome"]);
	unset($_SESSION["cognome"]);
	unset($_SESSION["ruolo"]);
	unset($_SESSION["punti"]);
	require("componenti/navbar/navbar.php");
	require("functions.php");
	require("php/bootstrap.php");
	?>

	<h2 id="title-form">Login Form</h2>

	<form method="POST" id="scatola_login">

		<div class="imgcontainer">
			<img src="image/centrale_home.png" alt="Avatar" class="avatar">
		</div>

		<div class="container">
			<label><?php
					if (isset($_SESSION["error_login"])) {
						echo $_SESSION["error_login"];
						unset($_SESSION["error_login"]);
						echo "<br>";
					} ?>
			</label>
			<fieldset>
				<label for="uname">Username</label>
				<input type="text" placeholder="Enter Username" id="uname" name="username" required>
				<br>
				<label for="psw">Password</label>
				<input type="password" placeholder="Enter Password" id="psw" name="password" required>
				<br>
				<div class="row justify-content-center">
					<div class="col"></div>
					<div class="col justify-content-center">
						<div class="btn-group btn-group-toggle" data-toggle="buttons">

							<label class="active btn btn-secondary" for="user">
								<input type="radio" name="ruolo" id="user" checked value="0"> Utente
							</label>
							<label class="btn btn-secondary" for="admin">
								<input type="radio" name="ruolo" id="admin" value="1"> Venditore
							</label>

						</div>
					</div>
					<div class="col"></div>
				</div>
				<button type="submit" id="login">Login</button>

				<a id="collegamento" href="iscrizione.php">Non sei ancora iscritto?</a>

		</div>
		</fieldset>
	</form>


	<?php
	if (isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["ruolo"])) {
		$login_result = $dbh->checkLogin($_POST["username"], $_POST["password"], $_POST["ruolo"]);
		if (count($login_result) == 0) {
			$_SESSION["error_login"] = "Errore! Username o password non corretti!";
			echo "<meta http-equiv='refresh' content='0'>";
		} else {
			if ($_POST["ruolo"]) {
				registerLoggedUser($login_result[0]);
				set_url("venditore.php");
			} else {
				if ($_GET["PAG"] == 1) {
					set_url("carrello.php");
				} elseif ($_GET["PAG"] == 2) {
					set_url("riepilogo.php");
				} else {
					set_url("profilo_utente.php");
				}
				registerLoggedUser($login_result[0]);
			}
		}
	}
	?>

	<?php
	require("componenti/footer/footer.php");
	?>

</body>
