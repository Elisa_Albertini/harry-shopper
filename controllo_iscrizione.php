<?php

if (isset($_POST["username"]) && isset($_POST["nome"]) && isset($_POST["cognome"]) && isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["indirizzo"])) {
    if($dbh->inserisciUtente($_POST["username"], $_POST["nome"], $_POST["cognome"], $_POST["email"], $_POST["password"], $_POST["indirizzo"])){
        $_SESSION["username"] = $_POST["username"];
        $_SESSION["nome"] = $_POST["nome"];
        $_SESSION["cognome"] = $_POST["cognome"];
        $_SESSION["punti"] = 0;
        $_SESSION["ruolo"] = 0;
    set_url("profilo_utente.php");
    }else{
        $_SESSION["error_iscr"] = "Il nome utente inserito è già in uso! Prova a sceglierne un altro!";
        echo "<meta http-equiv='refresh' content='0'>";
    }
}
?>
