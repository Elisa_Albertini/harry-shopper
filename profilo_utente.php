<!DOCTYPE html>
<html>

<head>
    <title>Harry Shopper - Profilo</title>
    <?php
    require("functions.php");
    require("dipendenze/dipendenze_head.php");
    if(!isset($_SESSION["username"])) :?>
    <?php set_url("login.php");
    elseif(!$_SESSION["ruolo"]) :
    unset($_SESSION["error_aggiunta_carrello"]);
    ?>

    <link rel="stylesheet" href="css/profilo.css">
    <script src="js/bottoni_profilo.js" type="text/javascript"></script>
</head>

<body>

    <?php
    require("componenti/navbar/navbar.php");
    ?>

    <h1 class="m-5">Profilo</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg">
            </div>
            <div class="col-12">
                <div class="card m-4">

                    <div class="card-body">
                        <img id="avatar" alt="avatar" src="https://avatar.oxro.io/avatar.svg?name=<?php echo $_SESSION["username"]; ?>">
                        Nome utente:
                        <h2 class="card-title text-center">
                            <?php echo $_SESSION["username"]; ?>
                        </h2>
                        Nome:
                        <h2 class="card-title text-center">
                            <?php echo $_SESSION["nome"]; ?>
                        </h2>
                        Cognome:
                        <h2 class="card-title text-center">
                            <?php echo $_SESSION["cognome"]; ?>
                        </h2>
                        Punti:
                        <h2 class="card-title text-center">
                            <?php echo $_SESSION["punti"]; ?>
                        </h2>

                        <div class="container-fluid">
                            <div class="row justify-content-center">
                                <div class="btn-group btn-block justify-content-center">
                                    <button type="button" id="carrello" class="btn btn-default col-md-3 m-3 btn-block">Mio carrello</button>
                                    <button type="button" id="riepilogo" class="btn btn-default col-md-3 m-3 btn-block">Riepilogo</button>
                                </div>
                            </div>
                        </div>

                        <p class="text-center m-2">
                            <a href="login.php" id="logout">Logout</a>
                        </p>

                    </div>
                </div>
            </div>
            <div class="col-lg">
            </div>
        </div>
    </div>


    <?php
    require("componenti/footer/footer.php");
    ?>

</body>
<?php elseif($_SESSION["ruolo"]): ?>
  <?php set_url("venditore.php");?>
<?php endif;?>
