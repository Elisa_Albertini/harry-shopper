<!DOCTYPE html>
<html>

<head>
<title>Harry Shopper - Prodotto</title>
    <?php
    require("dipendenze/dipendenze_head.php");
    require("functions.php");
    if(!isset($_GET["ID"])):
        set_url("insieme_prodotti.php?TIPO=none");
   ?>
    <?php
    elseif(isset($_SESSION["username"]) && !$_SESSION["ruolo"] || !isset($_SESSION["username"])) :
    ?>

    <script src="js/bottoni_prodotto.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css\pagina_prodotto.css">

</head>

<body>

    <?php
    require("componenti/navbar/navbar.php");
    require("php/bootstrap.php");

    $products = $dbh->infoProdotto($_GET["ID"]);

    if(!isset($_SESSION["username"])){
        $_SESSION["error_aggiunta_carrello"] = "Per acquistare un prodotto devi aver fatto prima log in";
    }
    ?>

    <div class="container mt-5">
        <div class="row m-4">

            <div class="col-md-5 mt-4">
                <img src="<?php echo UPLOAD_DIR . $products[0]["foto"]; ?>" alt="<?php echo $products[0]["nomeProdotto"]; ?>" class="image-responsive" />
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">

                <div class="row">
                    <div class="col-md-12">
                        <h1><?php echo $products[0]["nomeProdotto"]; ?></h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <span class="label label-primary">Tipologia: <?php echo $products[0]["nomeTipologia"]; ?></span>
                        <br>
                        <span class="label label-primary">Pezzi rimasti: <?php echo $products[0]["quantità"]; ?></span>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-12 bottom-rule">
                        <h2 class="product-price">Prezzo: <?php echo $products[0]["prezzo"]; ?> Galeoni</h2>
                        <p id="errore"><?php if (isset($_SESSION["error_aggiunta_carrello"])) {
                                echo $_SESSION["error_aggiunta_carrello"];
                                unset( $_SESSION["error_aggiunta_carrello"]);
                            } ?></p>
                    </div>
                </div>

                <br>
                <form method="POST">

                    <div class="col product-qty">

                        <div class="input-group mb-3">
                            <button id="meno" class="btn btn-outline-secondary" type="button">-</button>
                            <label for="numero" id="lblNum">valore</label>
                            <input type="number" id="numero" name="numero" class="form-control" value="1" max=<?php echo $dbh->getQuantity($products[0]["codProdotto"]) ?>>
                            <button id="piu" class="btn btn-outline-secondary" type="button">+</button>
                        </div>
                    </div>

                    <div class="col-md-4">

                        <button type="submit" class="btn btn-lg btn-brand btn-full-width" id="btnAggiunta">
                            Aggiungi al carrello
                        </button>
                    </div>
                </form>
                <?php require("aggiunta_prodotto_carrello.php"); ?>
                <br>
                <span><?php echo $products[0]["descrizione"]; ?></span>

            </div>
        </div>
    </div>

    <?php
    require("componenti/footer/footer.php");
    ?>

</body>
<?php elseif(isset($_SESSION["username"]) && $_SESSION["ruolo"]): ?>
  <?php set_url("venditore.php");?>
<?php endif;?>
