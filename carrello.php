<!DOCTYPE html>
<html>
<head>
    <title>Harry Shopper - Carrello</title>
    <?php
    require("dipendenze/dipendenze_head.php");
    require("functions.php");
    if(!isset($_SESSION["username"])) :?>
    <?php set_url("login.php");?>
    <?php
    elseif(!$_SESSION["ruolo"]) :
    ?>
    <script src="js/checkbox.js" type="text/javascript"></script>

    <link rel="stylesheet" href="css\carrello.css">
</head>

<body>

    <?php
    require("componenti/navbar/navbar.php");
    require("php/bootstrap.php");

    $product = $dbh->prodottiCarrello($_SESSION["username"]);
    $messaggioErrore = '0';
    if (empty($product)) {
        $messaggioErrore = "Carrello vuoto";
    }
    ?>

    <h1 class="m-4">Carrello</h1>
    <br>

    <div class="container">
        <div class="row">
            <div class="col col-lg-7">
                <div class="list-group m-4">
                    <label><?php if ($messaggioErrore != '0') {
                                echo $messaggioErrore;
                                $messaggioErrore = '0';
                            } ?></label>
                    <?php foreach ($product as $prod) : ?>
                        <div class="list-group-item list-group-item-action">
                            <div class=" justify-content-between">
                                <h2>Prodotto: <?php echo $prod["nomeProdotto"] ?></h2>
                            </div>
                            <h3>Quantità: <?php echo $prod["quantitàInserita"] ?></h3>
                            <div class="row">
                                <div class="col-md-8">
                                    <form method="POST">
                                        <div class="input-group m-1">
                                            <input type="hidden" id="codProd" name="codProd" class="form-control" value="<?php echo $prod["codProdotto"] ?>">
                                            <input type="hidden" id="data" name="data" class="form-control" value="<?php echo $prod["data_ora"] ?>">
                                            <button type="submit" class="btn btn-lg btn-brand btn-full-width" id="aggiunta">
                                                Elimina dal carrello
                                            </button>
                                            <?php
                                            require("eliminazione_prodotto_carrello.php");
                                            ?>


                                        </div>
                                    </form>
                                </div>
                                <div class="col text-right">
                                    <p class=" text-right">Prezzo: <?php echo $prod["prezzo"] ?> Galeoni</p>
                                </div>
                            </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="col-lg">
                <div class="card m-3">
                    <h2 class="card-header">Riepilogo ordine</h2>
                    <div class="card-body m-3">
                        <h2 class="card-title">Ordine</h2>
                        <small class="card-text">Destinatario</small>
                        <p class="card-text text-right"><?php echo $_SESSION["nome"]; ?> <?php echo $_SESSION["cognome"]; ?> <br>
                        </p>
                        <small class="card-text">Arrivo Previsto</small>
                        <p class="card-text text-right">Dipende dal gufo disponibile</p>
                        <form id="checkForm" method="POST">
                            <input type="checkbox" id="checkPunti" name="checkPunti" value="1" <?php if (isset($_POST['checkPunti'])) echo "checked='checked'"; ?> />
                            <label for="checkPunti"> Sconta punti se presenti</label><br>
                        </form>

                        <small class="card-text">Totale:</small>
                        <p class="card-text text-right"> <?php $_SESSION["totale"] = $dbh->totaleCarrello($_SESSION["username"]); echo $_SESSION["totale"] ?>
                            Galeoni
                        </p>
                        <br>
                        <form method="POST">
                            <small><label for="indirizzo">Vuoi inviare a un indirizzo diverso?</label></small>
                            <input type="text" id="indirizzo" placeholder="Inserire nuovo indirizzo" name="indirizzo">

                            <div class="m-4"></div>

                            <button type="submit" class="btn btn-lg btn-brand btn-full-width m-2" id="acquisto">
                                Procedi con l'acquisto
                            </button>
                            <?php
                            require("procedi_acquisto.php");
                            ?>
                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <?php
    require("componenti/footer/footer.php");
    ?>

</body>
<?php elseif($_SESSION["ruolo"]): ?>
  <?php set_url("venditore.php");?>
<?php endif;?>
